DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` integer NOT NULL AUTO_INCREMENT,
  `post_id` integer,
  `body` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO `comments` (`id`,`post_id`,`body`,`created_at`,`updated_at`) VALUES (10,6,'comment for post_id 6','2015-03-26 14:54:48.838999','2015-03-26 14:54:48.838999');
DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `id` integer NOT NULL AUTO_INCREMENT,
  `title` varchar(256),
  `body` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO `posts` (`id`,`title`,`body`,`created_at`,`updated_at`) VALUES (6,'test01','test0101','2015-03-26 14:54:01.045578','2015-03-26 14:54:01.045578');
INSERT INTO `posts` (`id`,`title`,`body`,`created_at`,`updated_at`) VALUES (7,'test02','test0202','2015-03-26 14:55:44.897007','2015-03-26 14:55:44.897007');
DROP TABLE IF EXISTS `schema_migrations`;
CREATE TABLE `schema_migrations` (
  `version` varchar(256) NOT NULL
);

CREATE UNIQUE INDEX unique_schema_migrations ON `schema_migrations` (version);
INSERT INTO `schema_migrations` (`version`) VALUES ('20150318084421');
INSERT INTO `schema_migrations` (`version`) VALUES ('20150318084451');